﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SEScriptReplacer
{
    public class BlueprintScriptParser
    {
        public List<ScriptContainer> ScriptContainers = new List<ScriptContainer>();

        string _originalText;
        StringBuilder _outputBuilder = new StringBuilder();
        List<string> _programBlockNodeStrings = new List<string>();

        const string ProgramBlockPattern = @"<MyObjectBuilder_CubeBlock xsi:type=""MyObjectBuilder_MyProgrammableBlock"">(.|\n)+?</MyObjectBuilder_CubeBlock>";
        const string ProgramNamePattern = @"<CustomName>(.+?)</CustomName>";
        const string ProgramContentPattern = @"<Program>((.|\n)+?)</Program>";
        const string ProgramIdPattern = @"<EntityId>([0-9]+?)</EntityId>";
        const string ProgramTemplate = "<Program>{0}</Program>";
        const string ProgramMarker = "#_PROGRAM_MARKER_#";

        public void ParseText(string originalText)
        {
            _originalText = originalText;
            GetProgramBlockNodes(_programBlockNodeStrings);
            GetProgramNameAndContents(_programBlockNodeStrings, ScriptContainers);
        }

        public void GetProgramBlockNodes(List<string> programBlockNodeStrings)
        {
            programBlockNodeStrings.Clear();

            var matches = Regex.Matches(_originalText, ProgramBlockPattern);
            foreach (Match match in matches)
            {
                programBlockNodeStrings.Add(match.Value);
            }
        }

        public void GetProgramNameAndContents(List<string> programBlockObNodeStrings, List<ScriptContainer> scriptContainers)
        {
            scriptContainers.Clear();

            foreach (string programObNode in programBlockObNodeStrings)
            {
                string name = Regex.Match(programObNode, ProgramNamePattern).Groups[1].Value;
                string programNode = Regex.Match(programObNode, ProgramContentPattern).Value;
                string programContent = Regex.Match(programObNode, ProgramContentPattern).Groups[1].Value;
                string programId = Regex.Match(programObNode, ProgramIdPattern).Groups[1].Value;

                //Console.WriteLine($"\nprogramNode: {programNode}\n");
                //Console.WriteLine($"\nprogramContent: {programContent}\n");

                string replaceTemplate = programObNode;
                if (!string.IsNullOrEmpty(programNode))
                    replaceTemplate = replaceTemplate.Replace(programNode, string.Format(ProgramTemplate, ProgramMarker));

                //Console.WriteLine($"\nReplace template: {replaceTemplate}\n");

                scriptContainers.Add(new ScriptContainer($"{name} (ID:{programId})", programContent, programObNode, replaceTemplate, ProgramMarker));

                Console.WriteLine($"Added script container: {name}");
            }

            scriptContainers.Sort((a, b) => a.ScriptName.CompareTo(b.ScriptName));
        }

        public string WriteUpdatedBlueprint()
        {
            _outputBuilder.Clear();
            _outputBuilder.Append(_originalText);

            foreach(var sc in ScriptContainers)
            {
                if (!string.IsNullOrEmpty(sc.OriginalObjectBuilder))
                    _outputBuilder.Replace(sc.OriginalObjectBuilder, sc.GetNewObjectBuilder());
            }

            return _outputBuilder.ToString();
        }
    }
}
