﻿namespace SEScriptReplacer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxFilePathBp = new System.Windows.Forms.TextBox();
            this.buttonLoadBp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonReplace = new System.Windows.Forms.Button();
            this.listBoxScripts = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxNewScript = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 37);
            this.label2.TabIndex = 2;
            this.label2.Text = "Blueprint";
            // 
            // textBoxFilePathBp
            // 
            this.textBoxFilePathBp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFilePathBp.Location = new System.Drawing.Point(12, 76);
            this.textBoxFilePathBp.Name = "textBoxFilePathBp";
            this.textBoxFilePathBp.ReadOnly = true;
            this.textBoxFilePathBp.Size = new System.Drawing.Size(367, 24);
            this.textBoxFilePathBp.TabIndex = 3;
            // 
            // buttonLoadBp
            // 
            this.buttonLoadBp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadBp.Location = new System.Drawing.Point(296, 39);
            this.buttonLoadBp.Name = "buttonLoadBp";
            this.buttonLoadBp.Size = new System.Drawing.Size(83, 34);
            this.buttonLoadBp.TabIndex = 4;
            this.buttonLoadBp.Text = "Load";
            this.buttonLoadBp.UseVisualStyleBackColor = true;
            this.buttonLoadBp.Click += new System.EventHandler(this.buttonLoadBp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(450, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 37);
            this.label3.TabIndex = 5;
            this.label3.Text = "New Script";
            // 
            // buttonReplace
            // 
            this.buttonReplace.Enabled = false;
            this.buttonReplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReplace.Location = new System.Drawing.Point(457, 390);
            this.buttonReplace.Name = "buttonReplace";
            this.buttonReplace.Size = new System.Drawing.Size(359, 34);
            this.buttonReplace.TabIndex = 8;
            this.buttonReplace.Text = "Replace";
            this.buttonReplace.UseVisualStyleBackColor = true;
            this.buttonReplace.Click += new System.EventHandler(this.buttonReplace_Click);
            // 
            // listBoxScripts
            // 
            this.listBoxScripts.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxScripts.FormattingEnabled = true;
            this.listBoxScripts.HorizontalScrollbar = true;
            this.listBoxScripts.ItemHeight = 17;
            this.listBoxScripts.Location = new System.Drawing.Point(12, 154);
            this.listBoxScripts.Name = "listBoxScripts";
            this.listBoxScripts.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxScripts.Size = new System.Drawing.Size(367, 310);
            this.listBoxScripts.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 37);
            this.label4.TabIndex = 10;
            this.label4.Text = "Script List";
            // 
            // textBoxNewScript
            // 
            this.textBoxNewScript.Enabled = false;
            this.textBoxNewScript.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNewScript.Location = new System.Drawing.Point(457, 76);
            this.textBoxNewScript.MaxLength = 0;
            this.textBoxNewScript.Multiline = true;
            this.textBoxNewScript.Name = "textBoxNewScript";
            this.textBoxNewScript.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxNewScript.Size = new System.Drawing.Size(359, 308);
            this.textBoxNewScript.TabIndex = 0;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // buttonSave
            // 
            this.buttonSave.Enabled = false;
            this.buttonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(457, 430);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(359, 34);
            this.buttonSave.TabIndex = 11;
            this.buttonSave.Text = "Save to Blueprint";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 489);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBoxScripts);
            this.Controls.Add(this.buttonReplace);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonLoadBp);
            this.Controls.Add(this.textBoxFilePathBp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNewScript);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.Text = "SE Script Replacer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxFilePathBp;
        private System.Windows.Forms.Button buttonLoadBp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonReplace;
        private System.Windows.Forms.ListBox listBoxScripts;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxNewScript;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonSave;
    }
}

