﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SEScriptReplacer
{
    public partial class MainForm : Form
    {
        #region Fields
        const string Version = "0.0.2.0";
        const string Date = "05/11/2019";
        bool _fileLoaded = false;
        string _filePath = "";
        string _rawText;
        BlueprintScriptParser _blueprintParser = new BlueprintScriptParser();
        List<string> _scriptNames = new List<string>();

        string[] _binaryFileExtensions = new string[] { "PB", "B1", "B2", "B3", "B4", "B5" };
        const string DefaultFileExtension = ".sbc";
        const string BackupFileExtension = " backup {0:0000}.{1:00}.{2:00} - {3:00}.{4:00}.{5:00}.sbc";

        #endregion

        #region Methods
        public MainForm()
        {
            InitializeComponent();

            //Set title
            this.Text = $"SE Script Replacer (v{Version} - {Date})";

            //Set file filter
            openFileDialog.Filter = "Blueprint Files (*.sbc) | *.sbc";

            //Set initial file directory
            var appDataLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            if (!string.IsNullOrWhiteSpace(appDataLocation))
            {
                var desiredDirectory = appDataLocation + @"\SpaceEngineers\Blueprints\local";
                if (Directory.Exists(desiredDirectory))
                    openFileDialog.InitialDirectory = desiredDirectory;
            }
        }

        void ParseBlueprint()
        {
            Console.WriteLine($"Parsing blueprint...");
            _rawText = File.ReadAllText(_filePath);
            _blueprintParser.ParseText(_rawText);

            _scriptNames.Clear();
            foreach (var sc in _blueprintParser.ScriptContainers)
            {
                _scriptNames.Add(sc.ScriptName);
            }

            listBoxScripts.DataSource = _scriptNames;

            _fileLoaded = true;
            buttonReplace.Enabled = true;
            buttonSave.Enabled = true;
            textBoxNewScript.Enabled = true;
            Console.WriteLine($"Parsed!");
        }

        void ReplaceScriptContent()
        {
            if (!_fileLoaded)
            {
                MessageBox.Show($"No file loaded!",
                        "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }

            Console.WriteLine($"Replacing script content...");

            string convertedScript = ScriptToXml.Convert(textBoxNewScript.Text);

            foreach (int idx in listBoxScripts.SelectedIndices)
            {
                _blueprintParser.ScriptContainers[idx].NewScript = convertedScript;
            }

            Console.WriteLine($"Replaced!");
        }

        void SaveBlueprint()
        {
            if (!_fileLoaded)
            {
                MessageBox.Show($"No file loaded!",
                        "WARNING", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                return;
            }

            Console.WriteLine($"Backing up original blueprint...");

            var now = DateTime.Now;
            string backupFileExtension = string.Format(BackupFileExtension, 
                                                       now.Year,
                                                       now.Month,
                                                       now.Day,
                                                       now.Hour,
                                                       now.Minute,
                                                       now.Second);

            File.WriteAllText(_filePath.Replace(DefaultFileExtension, backupFileExtension), _rawText);

            Console.WriteLine($"Saving updated blueprint...");
            string updatedOutput = _blueprintParser.WriteUpdatedBlueprint();
            File.WriteAllText(_filePath, updatedOutput);

            Console.WriteLine($"Removing ProtoBuffer files...");
            foreach (string extension in _binaryFileExtensions)
            {
                string binaryFileName = $"{_filePath}{extension}";
                if (File.Exists(binaryFileName))
                {
                    File.Delete(binaryFileName);
                    Console.WriteLine($"Binary file {binaryFileName} deleted");
                }
            }


            Console.WriteLine("Done!");
        }
        #endregion

        #region Events
        private void buttonLoadBp_Click(object sender, EventArgs e)
        {
            openFileDialog.DefaultExt = ".sbc";
            openFileDialog.ShowDialog();
        }
        #endregion

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            _filePath = openFileDialog.FileName;
            Console.WriteLine($"Loading \"{_filePath}\"...");
            textBoxFilePathBp.Text = _filePath;
            ParseBlueprint();
        }

        private void buttonReplace_Click(object sender, EventArgs e)
        {
            ReplaceScriptContent();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveBlueprint();
        }
    }
}
