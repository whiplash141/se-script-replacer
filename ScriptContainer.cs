﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEScriptReplacer
{
    public class ScriptContainer
    {
        public string NewScript;
        public readonly string ScriptName;
        public readonly string OriginalObjectBuilder;
        private readonly string _newObTemplate;
        private readonly string _replacementMarker;

        public ScriptContainer(string name, string script, string originalObjectBuilder, string newObjectBuilderTemplate, string replacementMarker)
        {
            ScriptName = name;
            NewScript = script;
            OriginalObjectBuilder = originalObjectBuilder;
            _newObTemplate = newObjectBuilderTemplate;
            _replacementMarker = replacementMarker;
        }

        public string GetNewObjectBuilder()
        {
            return _newObTemplate.Replace(_replacementMarker, NewScript);
        }
    }
}
