﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEScriptReplacer
{
    public static class ScriptToXml
    {
        static readonly StringBuilder _stringBuilder = new StringBuilder();

        static readonly Dictionary<string, string> _symbolDict = new Dictionary<string, string>()
        {
            { "&", "&amp;" },
            { ">", "&gt;" },
            { "<", "&lt;" },
        };

        public static string Convert(string input)
        {
            _stringBuilder.Clear();
            _stringBuilder.Append(input);

            foreach (var kvp in _symbolDict)
            {
                _stringBuilder.Replace(kvp.Key, kvp.Value);
            }

            return _stringBuilder.ToString();
        }
    }
}
